package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by User on 27/04/2017.
 */
public class DynamicRectangleShape extends RectangleShape {


    protected static final Color leftDefault = Color.red;
    protected static final Color rigtDefault = Color.blue;
    protected static final Status statDefault = Status.origin;

    protected Color left;
    protected Color right;
    protected Status status = statDefault;




    public DynamicRectangleShape() {
        super();
        this.left = leftDefault;
        this.right= rigtDefault;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
        this.left = leftDefault;
        this.right= rigtDefault;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
        this.left = leftDefault;
        this.right= rigtDefault;
    }
    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color leftw, Color rightw) {
        super(x, y, deltaX, deltaY, width, height);
        this.left = leftw;
        this.right = rightw;
    }

    @Override
    public void move(int width, int height) {
        int nextX = fX + fDeltaX;
        int nextY = fY + fDeltaY;

        if (nextX <= 0 || (nextX <= 0 && nextY <= 0) || (nextX <= 0 && nextY + fHeight >= height)) {
            nextX = 0;
            fDeltaX = -fDeltaX;
            status = Status.left;
        } else if (nextX + fWidth >= width || (nextX + fWidth >= width && nextY <= 0) || (nextX + fWidth >= width && nextY + fHeight >= height)) {
            nextX = width - fWidth;
            fDeltaX = -fDeltaX;
            status = Status.right;
        }

        if (nextY <= 0) {
            nextY = 0;
            fDeltaY = -fDeltaY;
            status = Status.origin;
        } else if (nextY + fHeight >= height) {
            nextY = height - fHeight;
            fDeltaY = -fDeltaY;
            status = Status.origin;
        }

        fX = nextX;
        fY = nextY;
    }



    @Override
    public void paint(Painter painter) {

        switch (status) {
            case origin: painter.drawRect(fX,fY,fWidth,fHeight);
                         break;
            case left: painter.drawRect(fX,fY,fWidth,fHeight);
                       painter.setColor(left);
                       painter.fillRect(fX,fY,fWidth,fHeight);
                       break;
            case right: painter.drawRect(fX,fY,fWidth,fHeight);
                        painter.setColor(right);
                        painter.fillRect(fX,fY,fWidth,fHeight);
                        break;
        }
}


}
