package ictgradschool.industry.lab12.bounce;

/**
 * Created by User on 27/04/2017.
 */
public class GemShape extends Shape{

    public GemShape() {
        super();
    }

    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }

    @Override
    public void paint(Painter painter) {

        int xPlusHalf = fX+(fWidth/2);
        int yPlusHalf = fY+(fHeight/2);
        int xPlusW = fX + fWidth;
        int yPlusH = fY + fHeight;
        if (this.fWidth<=40)
        {

            painter.drawLine(xPlusHalf, fY, fX, yPlusHalf);
            painter.drawLine(xPlusHalf,fY, xPlusW, yPlusHalf);
            painter.drawLine(xPlusW, yPlusHalf, xPlusHalf, yPlusH);
            painter.drawLine(fX, yPlusHalf, xPlusHalf, yPlusH);
        }
        else{
            painter.drawLine(fX, yPlusHalf, fX+20, fY);
            painter.drawLine(fX, yPlusHalf, fX+20, yPlusH);
            painter.drawLine(fX+20, fY, xPlusW-20,fY);
            painter.drawLine(fX+20, yPlusH, xPlusW-20,yPlusH);
            painter.drawLine(xPlusW-20,fY,xPlusW,yPlusHalf);
            painter.drawLine(xPlusW-20,yPlusH, xPlusW,yPlusHalf);

        }

    }




}
