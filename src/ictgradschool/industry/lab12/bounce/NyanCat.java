package ictgradschool.industry.lab12.bounce;

/**
 * Created by User on 27/04/2017.
 */
import java.awt.*;
import javax.imageio.*;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class NyanCat{

    protected static final Color[] rainbow = new Color[] {
            new Color(248, 12, 18),
            new Color(238, 17, 0),
            new Color(255, 51, 17),
            new Color(255, 68, 34),
            new Color(255, 102, 68),
            new Color(255, 153, 51),
            new Color(254, 174, 45),
            new Color(204, 187, 51),
            new Color(208, 195, 16),
            new Color(170, 204, 34),
            new Color(105, 208, 37),
            new Color(34, 204, 170),
            new Color(18, 189, 185),
            new Color(17, 170, 187),
            new Color(68, 68, 221),
            new Color(51, 17, 187),
            new Color(59, 12, 189),
            new Color(68, 34, 153)};


    private Image img = null;


    protected static final int DEFAULT_X_POS = 0;

    protected static final int DEFAULT_Y_POS = 0;

    protected static final int DEFAULT_DELTA_X = 5;

    protected static final int DEFAULT_DELTA_Y = 5;

    protected static final int DEFAULT_HEIGHT = 60;

    protected static final int DEFAULT_WIDTH = 80;

    protected int fX;

    protected int fY;

    protected int fDeltaX;

    protected int fDeltaY;

    protected int fWidth;

    protected int fHeight;

    public NyanCat() {
        this.fX = DEFAULT_X_POS;
        this.fY = DEFAULT_Y_POS;
        this.fDeltaX = DEFAULT_DELTA_X;
        this.fDeltaY = DEFAULT_DELTA_Y;
        this.fWidth = DEFAULT_WIDTH;
        this.fHeight = DEFAULT_HEIGHT;
        loadImage("nyan_cat.png");
    }

    public NyanCat(int x, int y, int deltaX, int deltaY) {
        this.fX = x;
        this.fY = y;
        this.fDeltaX = deltaX;
        this.fDeltaY = deltaY;
        this.fWidth = DEFAULT_WIDTH;
        this.fHeight = DEFAULT_HEIGHT;
        loadImage("nyan_cat.png");

    }

    public void loadImage (String filename)
    {
        try {
            img = ImageIO.read(new File(filename));
        } catch (IOException e) {
            System.out.println("no valid image found");
            System.exit(1);
        }
    }





    public void move(int width, int height) {
        int nextX = fX + fDeltaX;
        int nextY = fY + fDeltaY;

        if (nextX <= 0) {
            nextX = 0;
            fDeltaX = -fDeltaX;
        } else if (nextX + fWidth >= width) {
            nextX = width - fWidth;
            fDeltaX = -fDeltaX;
        }

        if (nextY <= 0) {
            nextY = 0;
            fDeltaY = -fDeltaY;
        } else if (nextY + fHeight >= height) {
            nextY = height - fHeight;
            fDeltaY = -fDeltaY;
        }

        fX = nextX;
        fY = nextY;
    }

    public void paint(Painter painter) {

        painter.drawImage(img, fX, fY, null);
        for(Color c : rainbow) {
                painter.drawLine(fX, fY, fX + fDeltaX, fY + fDeltaY);
            }

    }




}
